<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AhliwarisModel extends CI_Model {

	private $_table = "t_ahliwaris";
	function show(){	  
        $this->db->select('ah.*,peg.nama,stat.status');
        $this->db->from('t_ahliwaris ah');
        $this->db->join('t_peserta pes', 'pes.id_peserta = ah.id_peserta');
        $this->db->join('t_pegawai peg', 'peg.id_pegawai = pes.id_pegawai'); 
        $this->db->join('t_status stat', 'ah.status_nikah = stat.id_status');

    	$query = $this->db->get(); 
        return $query->result();
    }



    public function listPeserta()
    {
        $this->db->select('*');
        $this->db->from('t_peserta a');
        $this->db->join('t_pegawai c', 'c.id_pegawai = a.id_pegawai');
        //$this->db->where('(SELECT count(0) FROM t_pensiun where t_pensiun.id_peserta = a.id_peserta) = 0');    
        $query = $this->db->get();
        return $query->result();
        /*if ($query->num_rows() >0){ 
            foreach ($query->result() as $data) {                
                $listPeserta[] = $data;
            }
        return $listPeserta; 
        }*/
    }

    function save($data){
        return $this->db->insert($this->_table, $data);
    } 


    public function getById($id)
    {
        $this->db->select('a.*, b.id_peserta, c.nama');
        $this->db->from('t_ahliwaris a');
        $this->db->join('t_peserta b', 'b.id_peserta = a.id_peserta');
        $this->db->join('t_pegawai c', 'c.id_pegawai = b.id_pegawai');
        return $this->db->get_where('t_ahliwaris', ["a.id_waris" => $id])->row();
    }

    public function edit($id, $data)
    {
        $this->db->where('id_waris', $id);
        $this->db->update($this->_table, $data);
    }

    public function delete($id)
    {       
        $this->db->where('id_waris', $id);
        $this->db->delete($this->_table);
    }
   
}