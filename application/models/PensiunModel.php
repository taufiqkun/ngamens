<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PensiunModel extends CI_Model {

	private $_table = "t_pensiun";
	function show(){	  
        $this->db->select('*');
        $this->db->from('t_pensiun a');
        $this->db->join('t_peserta b', 'b.id_peserta = a.id_peserta');
        $this->db->join('t_pegawai c', 'c.id_pegawai = b.id_pegawai'); 

        $listPensiun = [];    
    	$query = $this->db->get(); 
        if ($query->num_rows() > 0) { 
            foreach ($query->result() as $data) {                
                $listPensiun[] = $data;
            }
            return $listPensiun; 
        } 
    }

    public function showPeserta()
    {
        $this->db->select('*');
        $this->db->from('t_peserta a');
        $this->db->join('t_pegawai c', 'c.id_pegawai = a.id_pegawai');
        $this->db->where('(SELECT count(0) FROM t_pensiun where t_pensiun.id_peserta = a.id_peserta) = 0');    
        $query = $this->db->get();
        if ($query->num_rows() >0){ 
            foreach ($query->result() as $data) {                
                $listPeserta[] = $data;
            }
        return $listPeserta; 
        }
    }

    

    function getById($id){
	    $this->db->select("a.id_peserta,b.nik,b.id_pegawai, b.status_pegawai, STR_TO_DATE(b.awal_masuk, '%Y-%m-%d') as awal_tetap, b.status_nikah,DATE_FORMAT(a.tanggal_pensiun, '%d %M %Y') as tanggal_pensiun,
	    	ROUND(TIMESTAMPDIFF(MONTH,DATE_FORMAT(b.tanggal_lahir, '%Y-%m-%d'),STR_TO_DATE(NOW(), '%Y-%m-%d'))/12,2) AS usia,
	    	ROUND(TIMESTAMPDIFF(MONTH,STR_TO_DATE(b.awal_masuk, '%Y-%m-%d'),DATE_FORMAT(NOW(), '%Y-%m-%d'))/12,2) AS masaKerja"); 
	    $this->db->from('t_peserta a');
	    $this->db->join('t_pegawai b', 'a.id_pegawai = b.id_pegawai');
	    return $this->db->get_where('t_peserta', ["a.id_peserta" => $id])->row(); 
    } 

    function getStatusByIdPeg($id){
	    $this->db->select("status_nikah"); 
        $this->db->from("t_pegawai");
        $this->db->where("id_pegawai",$id);
        return $this->db->get()->row();
    } 

    function save($data){
        return $this->db->insert($this->_table, $data);
    } 

    public function delete($id)
    {
       
        $this->db->where('id_pensiun', $id);
        $this->db->delete($this->_table);
    }


    function getPensiunById($id)
    {
        $this->db->select("a.*,c.nik, DATE_FORMAT(b.tanggal_pensiun, '%d %M %Y') as tanggal_pensiun, c.nama, STR_TO_DATE(c.awal_tetap, '%Y-%m-%d') as awal_tetap, STR_TO_DATE(c.awal_masuk, '%Y-%m-%d') as awal_masuk, DATE_FORMAT(c.tanggal_lahir, '%d %M %Y') as tanggal_lahir,
            ROUND(TIMESTAMPDIFF(MONTH,DATE_FORMAT(c.tanggal_lahir, '%Y-%m-%d'),STR_TO_DATE(NOW(), '%Y-%m-%d'))/12,2) AS usia,
            ROUND(TIMESTAMPDIFF(MONTH,STR_TO_DATE(c.awal_masuk, '%Y-%m-%d'),DATE_FORMAT(NOW(), '%Y-%m-%d'))/12,2) AS masaKerja,
            ROUND(a.pensiun_bulanan * 0.2, 2) as persen20,
            ROUND(a.manfaat_bulanan * 0.8, 2) as persen80,
            DATE_FORMAT(b.tanggal_terima_pensiun, '%d %M %Y') as tanggal_terima_pensiun, c.status_pegawai, c.status_nikah"); 
        $this->db->from('t_pensiun a');
        $this->db->join('t_peserta b', 'b.id_peserta = a.id_peserta');
        $this->db->join('t_pegawai c', 'c.id_pegawai = b.id_pegawai');
        return $this->db->get_where('t_pensiun', ["a.id_pensiun" => $id])->row();
       

         
        // if ($query->num_rows() >0){ 
        //     foreach ($query->result() as $data) {                
        //         $result[] = $data;
        //     }
        // }
        // return $result; 
    }



}

/* End of file PensiunModel.php */
/* Location: ./application/models/PensiunModel.php */