<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PegawaiModel extends CI_Model {
	private $_table = "t_pegawai";
 

    function show(){
	    $this->db->select('*'); 
	    $this->db->from($this->_table); //memeilih tabel
	    
 
    	$query = $this->db->get(); //simpan database yang udah di get alias ambil ke query
        if ($query->num_rows() >0){ //membuat data masuk ke $data kemudian masuk lagi ke array $hasiltransaksi
            foreach ($query->result() as $data) {                
                $listPegawai[] = $data;
            }
        return $listPegawai; //hasil dari semua proses ada dimari
        }
    }  
    

      

    function total_pensiun()
    {
        $query = $this->db->get('t_pensiun');
        return $query->num_rows();
    }

     function total_pegawai()
    {
        $query = $this->db->get('t_pegawai');
        return $query->num_rows();
    }

     function total_peserta()
    {
        $query = $this->db->get('t_peserta');
        return $query->num_rows();
    }

    /*public function getById($id)
    {
        return $this->db->get_where($this->_table, ["product_id" => $id])->row();
    }

    public function save()
    {
        $post = $this->input->post();
        $this->product_id = uniqid();
        $this->name = $post["name"];
        $this->price = $post["price"];
        $this->description = $post["description"];
        $this->db->insert($this->_table, $this);
    }

    public function update()
    {
        $post = $this->input->post();
        $this->product_id = $post["id"];
        $this->name = $post["name"];
        $this->price = $post["price"];
        $this->description = $post["description"];
        $this->db->update($this->_table, $this, array('product_id' => $post['id']));
    }

    public function delete($id)
    {
        return $this->db->delete($this->_table, array("product_id" => $id));
    }
	*/

}

/* End of file PegawaiModel.php */
/* Location: ./application/models/PegawaiModel.php */