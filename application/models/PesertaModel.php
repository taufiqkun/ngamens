<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PesertaModel extends CI_Model {

    private $_tableahli = "t_ahliwaris";
    private $_tablepeg = "t_pegawai";
	private $_table = "t_peserta";
    public $id_pegawai;
    public $no_peserta;
    public $tanggal_pensiun;
    public $tanggal_terima_pensiun;
    public $status_peserta;
 

    function show(){	  
        $this->db->select('a.*, b.nama');
        $this->db->from('t_peserta a');  
        $this->db->join('t_pegawai b', 'b.id_pegawai = a.id_pegawai');           
 
    	$query = $this->db->get(); 
        if ($query->num_rows() > 0) { 
            foreach ($query->result() as $data) {                
                $listPeserta[] = $data;
            }
            return $listPeserta; 
        } 
    }  
    

    function total_peserta()
    {
        $query = $this->db->get($this->_table);
        if ($query->num_rows() > 0 )
        {
           $records = $query->result_array();
           $data['countPeserta'] = count($records);
           return $data;
        } 
    }

    function getPegawai()
    {
        $this->db->select('id_pegawai, nama');
        $this->db->from('t_pegawai'); 
        $this->db->where('(select COUNT(*) from t_peserta where t_peserta.id_pegawai = t_pegawai.id_pegawai) = 0'); 
        $this->db->order_by("nama", "asc");          
 
        $query = $this->db->get(); 
        if ($query->num_rows() > 0) { 
            foreach ($query->result() as $data) {                
                $listPegawai[] = $data;
            }
            return $listPegawai; 
        } 
    }
    

    public function save($data)
    {
        return $this->db->insert($this->_table, $data);
    }

    public function getById($id)
    {
        $this->db->select('a.*, b.nama');
        $this->db->from('t_peserta a'); 
        $this->db->join('t_pegawai b', 'b.id_pegawai = a.id_pegawai'); 
        $this->db->where('a.id_peserta', $id);
        return $this->db->get()->row();
    }

    public function update($id, $data)
    {
        $this->db->where('id_peserta', $id);
        $this->db->update($this->_table, $data);
    }

    public function delete($id)
    {
       
        $this->db->where('id_peserta', $id);
        $this->db->delete($this->_table);
    }

    public function getTglLahir($id)
    {        
        $this->db->select('tanggal_lahir,status_pegawai');
        $this->db->from('t_pegawai'); 
        $this->db->where('t_pegawai.id_pegawai',$id);
        return $this->db->get()->row();
    }

    public function getTglPensiun($stt,$tglLahir)
    {        
        if ($stt=="Dosen Tetap") {
             $sql="select date_add('$tglLahir', INTERVAL 65 YEAR) as tgl_pensiun";
        }else if ($stt=="Profesor") {
             $sql="select date_add('$tglLahir', INTERVAL70 YEAR) as tgl_pensiun";
        }else{
            $sql="select date_add('$tglLahir', INTERVAL 56 YEAR) as tgl_pensiun";
        }        
        $query = $this->db->query($sql);
        return $query->row();
    }

    public function addAhliWaris($data)
    {        
        return $this->db->insert($this->_tableahli, $data);
    }

}

/* End of file PesertaModel.php */
/* Location: ./application/models/PesertaModel.php */