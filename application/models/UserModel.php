<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UserModel extends CI_Model {

	public function get($username)
	{
		$this->db->where('username', $username); // Untuk menambahkan Where Clause : username='$username'
        $result = $this->db->get('t_user')->row(); // Untuk mengeksekusi dan mengambil data hasil query
        return $result;
	}

	public function register($data)
	{
	    return $this->db->insert('t_user', $data);
	}
	

}

/* End of file UserModel.php */
/* Location: ./application/models/UserModel.php */