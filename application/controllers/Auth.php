<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

class Auth extends CI_Controller {

	public function __construct($config = 'rest'){
	    parent::__construct($config);
	    $this->load->model('UserModel');
  	}

	public function index()
	{
		if($this->session->userdata('authenticated')) // Jika user sudah login (Session authenticated ditemukan)
      	redirect('home'); // Redirect ke page welcome
    	$this->load->view('login'); // Load view login.php
	}

	public function register()
	{
		$this->load->view('register');
	}


	public function login($value='')
	{
		$username = $this->input->post('username'); // Ambil isi dari inputan username pada form login
	    $password = md5($this->input->post('password')); // Ambil isi dari inputan password pada form login dan encrypt dengan md5 ==> md5($this->input->post('password'));
	    $user = $this->UserModel->get($username); // Panggil fungsi get yang ada di UserModel.php

	    

	    if(empty($user)){ // Jika hasilnya kosong / user tidak ditemukan
	      $this->session->set_flashdata('message', 'Username tidak ditemukan'); // Buat session flashdata
	      redirect('auth'); // Redirect ke halaman login
	    }else{
	      if($password == $user->password){ // Jika password yang diinput sama dengan password yang didatabase
	        $session = array(
	          'authenticated'=>true, // Buat session authenticated dengan value true
	          'username'=>$user->username,  // Buat session username
	          'nama'=>$user->nama_user, // Buat session authenticated
	          'id'=>$user->id_user
	        );
	        $this->session->set_userdata($session); // Buat session sesuai $session
	        redirect('home'); // Redirect ke halaman welcome
	      }else{
	        $this->session->set_flashdata('message', 'Password salah'); // Buat session flashdata
	        redirect('auth'); // Redirect ke halaman login
	      }
	    }
	}

	public function cek_register(){
		$nama = $this->input->post('nama');
		$username = $this->input->post('username');
		$password = md5($this->input->post('password'));
		$repassword = md5($this->input->post('repassword'));

		$cekUsername = $this->UserModel->get($username);
		if (!empty($cekUsername)) {
			$this->session->set_flashdata('message', 'Username sudah digunakan');
			redirect('auth/register','refresh');
		}
		else if($password != $repassword){
			$this->session->set_flashdata('message', 'Field Password tidak sama');
			redirect('auth/register','refresh');
		}
		else {
			$data = array(
			'nama_user' => $nama,
			'username' => $username,
			'password' => $password
			);

			$this->UserModel->register($data);

			$this->session->set_flashdata('success', 'Sign In success');
			redirect('auth');
		}
	}

	public function logout(){
    	$this->session->sess_destroy(); // Hapus semua session
   		redirect('auth'); // Redirect ke halaman login
   	}

}

/* End of file Auth.php */
/* Location: ./application/controllers/Auth.php */
