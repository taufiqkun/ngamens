<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Peserta extends CI_Controller {

	public function __construct($config = 'rest'){
	    parent::__construct($config);
        $this->load->model('PesertaModel');        
        $this->load->model('PensiunModel');        
	    $this->load->model('AhliwarisModel');        
        $this->load->library("session");
	    
  	}

  	public function index()
	{
        $data['title'] = 'Peserta';
		$data['listPeserta']=$this->PesertaModel->show();

        $data['listPegawai']=$this->PesertaModel->getPegawai();
		$data['id_peserta_from_ah']=$this->AhliwarisModel->show();

        $this->load->view('admin/peserta/peserta',$data);
  		// echo json_encode($data);
	}

	public function add()
    {
        $no_peserta = $this->input->post('no_peserta');
        $id_pegawai = $this->input->post('id_pegawai');
        $status_peserta = $this->input->post('status_peserta');
        $tanggal_pensiun = $this->input->post('tanggal_pensiun');
        $tanggal_terima_pensiun = $this->input->post('tanggal_terima_pensiun');

        $data = array(
			'no_peserta' => $no_peserta,
			'id_pegawai' => $id_pegawai,
			'status_peserta' => $status_peserta,
			'tanggal_pensiun' => $tanggal_pensiun,
			'tanggal_terima_pensiun' => $tanggal_terima_pensiun
			);

        $this->PesertaModel->save($data);
        $this->session->set_flashdata('message', 'Data Berhasil Disimpan...');
        redirect('peserta/peserta');
    }

    public function getById()
    {
    	$id = $this->input->post('id');
    	$data = $this -> PesertaModel -> getById($id);

    	echo json_encode($data);
    }

    public function update()
    {
    	$id = $this->input->post('id_peserta');
    	$no_peserta = $this->input->post('no_peserta');
        $id_pegawai = $this->input->post('id_pegawai');
        $status_peserta = $this->input->post('status_peserta');
        $tanggal_pensiun = $this->input->post('tanggal_pensiun');
        $tanggal_terima_pensiun = $this->input->post('tanggal_terima_pensiun');

        $data = array(
			'no_peserta' => $no_peserta,
			'id_pegawai' => $id_pegawai,
			'status_peserta' => $status_peserta,
			'tanggal_pensiun' => $tanggal_pensiun,
			'tanggal_terima_pensiun' => $tanggal_terima_pensiun
			);
        $this->PesertaModel->update($id, $data);
        $this->session->set_flashdata('message', 'Data Berhasil Diupdate...');
        redirect('peserta/peserta');
    }

    function delete()
    {
    	$this->PesertaModel->delete($this->input->post('id_peserta'));
        $this->session->set_flashdata('message', 'Data Berhasil Dihapus...');
        redirect('peserta/peserta');
    }

    function getTglPensiun(){
        $id = $this->input->post('id');
        $tglLahir = $this -> PesertaModel -> getTglLahir($id);
        $data = $this -> PesertaModel -> getTglPensiun($tglLahir->status_pegawai, $tglLahir->tanggal_lahir);

        echo json_encode($data);
    }

    function addAhliWaris(){
        $data = array(
            'id_peserta' => $this->input->post('idPeserta'),
            'nama_ahliwaris' => $this->input->post('namaAhliWaris'),
            'noktp_ahliwaris' => $this->input->post('noIdentitasAhliWaris'),
            'jk_ahliwaris' => $this->input->post('jkelAhliwaris'),
            'status_nikah' => $this->input->post('statusAhliwaris'),
            'pekerjaan' => $this->input->post('pekerjaanAhliWaris'),
            'pendidikan' => $this->input->post('pendidikanAhliwaris'),
            'kota_lahir_ahli_waris' => $this->input->post('tempatLahirAhliWaris'),
            'tanggal_lahir_ahli_waris' => $this->input->post('tglLahirAhliWaris'),
            'hubungan_keluarga' => $this->input->post('hubunganAhliwaris'),
            );

        $res =array(
        'result' =>$this -> PesertaModel -> addAhliWaris($data)
        );
        redirect('peserta/peserta');
    }

    function cekPensiun()
    {
        $id = $this->input->post('id');
        $pensiun=$this->PensiunModel->show();
        $data = array(
            'idkirim'   => $id,
            'onora'     =>'not',
            'idterima'  =>0,
        );
        foreach ($pensiun as $dt) {
           if ($dt->id_peserta==$id) {
                $data['onora']='ok';
                $data['idterima']=$dt->id_peserta;
            }
        }
        echo json_encode($data);
    }
    

	

}

/* End of file Peserta.php */
/* Location: ./application/controllers/peserta/Peserta.php */