<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pensiun extends CI_Controller {

	public function __construct($config = 'rest'){
	    parent::__construct($config);
	    $this->load->model('PensiunModel');
	    $this->load->library('Pdf');
  	}

	public function index()
	{
		$data['title'] = 'Pensiun';
		$data['listPensiun']=$this->PensiunModel->show();
		$data['listPeserta']=$this->PensiunModel->showPeserta();
		
        $this->load->view('admin/pensiun/pensiun',$data);
        // echo json_encode($data);
	}

	public function getById()
    {
    	$id = $this->input->post('id');
    	$data = $this -> PensiunModel -> getById($id);

    	echo json_encode($data);
	}
	


	public function add()
    {
        $id_peserta = $this->input->post('id_peserta');
        $tmt_masuk = $this->input->post('tmt_masuk');
        $usia = floatval($this->input->post('usia'));
        $masaKerja = floatval($this->input->post('masaKerja'));
        $nilai = floatval($this->input->post('nilai'));
        $faktor_sekaligus = floatval($this->input->post('faktor_sekaligus'));
        $fhdp = intval($this->input->post('fhdp')); 
        $status_nikah = $this->input->post('status_nikah');      
        $tanggal_berhenti =   $this->input->post('tanggal_pensiun'); 
        $status_pegawai =   $this->input->post('status_pegawai');  

       	$manfaat_bulanan = 0; $pensiun_bulanan = 0; $pensiun_total = 0;
		$keterangan_manfaat = "normal";

        if($status_nikah == 1 || $status_nikah == 2){		// menikah / jomblo
        	$manfaat_bulanan = round($nilai * (0.02 * $masaKerja * $fhdp), 2);
        	$pensiun_bulanan = round($faktor_sekaligus * $manfaat_bulanan, 2);
        	$pensiun_total = round($manfaat_bulanan + $pensiun_bulanan, 2);        	    	
        } 
        else if($status_nikah == 3){ 		// janda / duda
        	$manfaat_bulanan = round($nilai * (0.02 * $masaKerja * $fhdp), 2);
        	$pensiun_bulanan = round($faktor_sekaligus * $manfaat_bulanan * 0.75, 2);
        	$pensiun_total = round($manfaat_bulanan + $pensiun_bulanan, 2);        	
        }
        else{					// mokad
        		// todo ??
        } 

        if($status_pegawai == "Dosen Tetap"){
            if(floatval($usia) > 65.00){
                $keterangan_manfaat = "ditunda";
            }
            else if(floatval($usia) < 65.00){
                $keterangan_manfaat = "dipercepat";
            }
            else if(floatval($usia) == 65.00){
                $keterangan_manfaat = "normal";
            }
        } else if($status_pegawai == "Profesor"){
            if(floatval($usia) > 70.00){
                $keterangan_manfaat = "ditunda";
            }
            else if(floatval($usia) < 70.00){
                $keterangan_manfaat = "dipercepat";
            }
            else if(floatval($usia) == 70.00){
                $keterangan_manfaat = "normal";
            }
        } else{            
            if(floatval($usia) > 56.00){
                $keterangan_manfaat = "ditunda";
            }
            else if(floatval($usia) < 56.00){
                $keterangan_manfaat = "dipercepat";
            }
            else if(floatval($usia) == 56.00){
                $keterangan_manfaat = "normal";
            }
        }

        $data = array(
			'id_peserta'		=>$id_peserta,
			'keterangan_manfaat'=>strtoupper($keterangan_manfaat),
			'manfaat_bulanan' 	=>$manfaat_bulanan,
			'pensiun_bulanan' 	=>$pensiun_bulanan,
			'pensiun_total' 	=>$pensiun_total,
			'tanggal_berhenti'	=>$tanggal_berhenti,
			'nilai_sekarang'	=>$nilai,
			'faktor_sekaligus'	=>$faktor_sekaligus,
			'phdp'				=>$fhdp,
			'tanggal_insert' 	=>date('Y-m-d')
		);

        $this->PensiunModel->save($data);
		$hasil = $this->session->set_flashdata('message', 'Data Berhasil Disimpan...');		
        redirect('pensiun/pensiun');
    }

	public function cetak($id)
    {   	
    	$this->load->library('helper');
        $res['title'] = 'PDF';
    	$d = $this->PensiunModel->getPensiunById($id);

    	//$tes = explode('.',$this->helper->idr_format($d->persen20));
        if ($d->keterangan_manfaat!="NORMAL") {
            $besar_manfaat=$this->helper->idr_format($d->nilai_sekarang*(0.02*$d->masaKerja*$d->phdp));
        }else{            
            $besar_manfaat=$this->helper->idr_format(0.02*$d->masaKerja*$d->phdp);
        }
        $fakktor_sekaligus20=round($d->pensiun_bulanan*0.2,10);
        $fakktor_sekaligus80=round($d->manfaat_bulanan*0.8,10);

    	$res['data'] = array (
            'nama' => $d->nama,
    		'nik' => ($d->nik == "") ? "-" : $d->nik,
    		'tanggal_lahir' => $d->tanggal_lahir,
            'awal_tetap' => $d->awal_tetap,
    		'awal_masuk' => $d->awal_masuk,
    		'usia' => $d->usia,
    		'masaKerja' => $d->masaKerja,
    		'nilai_sekarang' => $d->nilai_sekarang,
    		'faktor_sekaligus' => $d->faktor_sekaligus,
    		'phdp' => $this->helper->idr_format($d->phdp),
    		'manfaat_bulanan' => $this->helper->idr_format($d->manfaat_bulanan),
    		'pensiun_bulanan' => $this->helper->idr_format($d->pensiun_bulanan),
    		'pensiun_total'=> $this->helper->idr_format($d->pensiun_total),
    		'tanggal_pensiun' => $d->tanggal_pensiun,
    		'persen20' => $this->helper->idr_format($d->persen20),
    		'persen80' => $this->helper->idr_format($d->persen80),
    		'tanggal_terima_pensiun' => $d->tanggal_terima_pensiun,
    		'timestamp' => date('d F Y'),
            'status_pegawai' => ($d->status_pegawai == "")? "-" : $d->status_pegawai,
            'keterangan' => $d->keterangan_manfaat,
            'besar_manfaat' => $this->helper->idr_format($d->manfaat_bulanan),
            'faktor_sekaligus_bulanan' => $this->helper->idr_format($d->pensiun_bulanan),
            'faktor_sekaligus20' => number_format($fakktor_sekaligus20,2,',','.'),
            'faktor_sekaligus80' => number_format($fakktor_sekaligus80,2,',','.'),
    	);
    	// echo json_encode($res);
    	$this->load->view('admin/pensiun/cetak', $res);
    }


    public function cetaklagi($id)
    {   	
    	//$this->load->view('admin/pensiun/ceklagi/);
    }

    function delete()
    {
    	$this->PensiunModel->delete($this->input->post('id_pensiun'));
        $this->session->set_flashdata('message', 'Data Berhasil Dihapus...');
        redirect('pensiun/pensiun');
    }
	
}

/* End of file Pensiun.php */
/* Location: ./application/controllers/pensiun/Pensiun.php */