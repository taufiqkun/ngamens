<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct($config = 'rest'){
	    parent::__construct($config);
	    $this->load->model('PegawaiModel');
  	}

	public function index(){
		$data['title'] = 'Dashboard';
		$a = $this->PegawaiModel->total_pegawai();
		$b = $this->PegawaiModel->total_peserta();
		$c = $this->PegawaiModel->total_pensiun();

		$data['total'] = array(
			'pensiun' => $c,
			'peserta' => $b,
			'pegawai' => $a
		);
		
		//echo json_encode($c);
    	$this->load->view('admin/home/home', $data);
  	}

}

/* End of file Home.php */
/* Location: ./application/controllers/Home.php */