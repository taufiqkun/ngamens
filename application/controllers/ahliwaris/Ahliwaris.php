<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ahliwaris extends CI_Controller {

	public function __construct($config = 'rest'){
	    parent::__construct($config);
	    $this->load->model('AhliwarisModel');

	}

	public function index()
	{
		$data = array(
			'title' 		=>  'ahliwaris', 
			'listAhliwaris' =>  $this->AhliwarisModel->show(),
			'listPeserta' 	=>  $this->AhliwarisModel->listPeserta()
		);
		// echo json_encode($data);	
        $this->load->view('admin/ahliwaris/ahliwaris',$data);  	
	}

	public function add()
	{
		$id_peserta = $this->input->post('id_peserta');
		$namaAhliWaris = $this->input->post('namaAhliWaris');
		$noIdentitasAhliWaris = $this->input->post('noIdentitasAhliWaris');
		$gender = $this->input->post('gender');
		$pekerjaanAhliWaris = $this->input->post('pekerjaanAhliWaris');
		$pendidikanAhliwaris = $this->input->post('pendidikanAhliwaris');
		$tempal_lahir = $this->input->post('tempal_lahir');
		$tglLahirAhliWaris = $this->input->post('tglLahirAhliWaris');
		$hubunganAhliwaris = $this->input->post('hubunganAhliwaris');
		$statusAhliwaris = $this->input->post('statusAhliwaris');


		$data = array(
			'id_peserta'		=>$id_peserta,
			'nama_ahliwaris'=>$namaAhliWaris,
			'noktp_ahliwaris' 	=>$noIdentitasAhliWaris,
			'jk_ahliwaris' 	=>$gender,
			'status_nikah' 	=>$statusAhliwaris,
			'pekerjaan'	=>$pekerjaanAhliWaris,
			'pendidikan'	=>$pendidikanAhliwaris,
			'kota_lahir_ahli_waris'	=>$tempal_lahir,
			'tanggal_lahir_ahli_waris'	=>$tglLahirAhliWaris,
			'hubungan_keluarga' 	=>$hubunganAhliwaris
		);

        $this->AhliwarisModel->save($data);
		$hasil = $this->session->set_flashdata('message', 'Data Berhasil Disimpan...');		
        redirect('ahliwaris/ahliwaris');
	}

	public function getById($id)
	{
		$data = $this -> AhliwarisModel -> getById($id);
    	echo json_encode($data);
	}

	public function edit()
	{	
		$id = $this->input->post('idAhliwaris');
		$id_peserta = $this->input->post('id_peserta');
		$namaAhliWaris = $this->input->post('namaAhliWaris');
		$noIdentitasAhliWaris = $this->input->post('noIdentitasAhliWaris');
		$gender = $this->input->post('jkelAhliwaris');
		$pekerjaanAhliWaris = $this->input->post('pekerjaanAhliWaris');
		$pendidikanAhliwaris = $this->input->post('pendidikanAhliwaris');
		$tempal_lahir = $this->input->post('tempatLahirAhliWaris');
		$tglLahirAhliWaris = $this->input->post('tglLahirAhliWaris');
		$hubunganAhliwaris = $this->input->post('hubunganAhliwaris');
		$statusAhliwaris = $this->input->post('statusAhliwaris');


		$data = array(
			'id_peserta'		=>$id_peserta,
			'nama_ahliwaris'=>$namaAhliWaris,
			'noktp_ahliwaris' 	=>$noIdentitasAhliWaris,
			'jk_ahliwaris' 	=>$gender,
			'status_nikah' 	=>$statusAhliwaris,
			'pekerjaan'	=>$pekerjaanAhliWaris,
			'pendidikan'	=>$pendidikanAhliwaris,
			'kota_lahir_ahli_waris'	=>$tempal_lahir,
			'tanggal_lahir_ahli_waris'	=>$tglLahirAhliWaris,
			'hubungan_keluarga' 	=>$hubunganAhliwaris
		);

        $this->AhliwarisModel->edit($id, $data);
		$hasil = $this->session->set_flashdata('message', 'Data Berhasil Disimpan...');		
        redirect('ahliwaris/ahliwaris');
	}

	function delete()
    {
    	$this->AhliwarisModel->delete($this->input->post('id'));
        $this->session->set_flashdata('message', 'Data Berhasil Dihapus...');
        redirect('ahliwaris/ahliwaris');
    }

}