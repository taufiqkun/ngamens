<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pegawai extends CI_Controller {

	public function __construct($config = 'rest'){
	    parent::__construct($config);
	    $this->load->model('PegawaiModel');

	}

	public function index()
	{
		$data['title'] = 'Pegawai';
		 $data['listPegawai']=$this->PegawaiModel->show();		
        $this->load->view('admin/pegawai/pegawai',$data);
  	
	}

}

/* End of file Pegawai.php */
/* Location: ./application/controllers/pegawai/Pegawai.php */