<!DOCTYPE html>
<html>
<head>
  <title>Sign In</title>
   
  <!--Bootsrap 4 CDN-->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous"> 

  <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">

  <!--Fontawesome CDN-->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <!------ Include the above in your HEAD tag ---------->


  <!--Custom styles-->
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/login.css"/>
</head>
<style>
  body{
    background-image: url("<?php echo base_url('assets/af.jpg') ?>");
  }
</style>
<body background-image="<?php echo base_url('assets/h1.png') ?>">

<div class="container">
  <div class="d-flex justify-content-center h-100">
      <div style="color: red;text-align: center;">
      <?php
      // Cek apakah terdapat session nama message
      if($this->session->flashdata('message')){ // Jika ada
        echo $this->session->flashdata('message'); // Tampilkan pesannya
      }
      ?>
      </div>

      <div style="color: green;text-align: center;">
      <?php
      // Cek apakah terdapat session nama message
      if($this->session->flashdata('success')){ // jika success
        echo $this->session->flashdata('success'); 
      }
      ?>
      </div>
    <div class="container">
<!-- role="form" -->
      <form class="form-signin" method="post" action="<?php echo base_url('auth/login') ?>">
         
        <h2 class="form-signin-heading"><center>SISTEM INFORMASI DANA PENSIUN</center></h2>
        <h5 class="baru"></h5>
        <center><img src="<?php echo base_url('assets/h1.png') ?>" width="100px" height="100px">
        <input name="username" id="user" style="width: 250px" type="input" class="form-control" placeholder="Username" required autofocus>
        <input name="password" id="pass" style="width: 250px" type="password" class="form-control" placeholder="Password" required>
       
        <button class="btn btn-lg btn-danger btn-block" type="submit" style="width: 250px">LOGIN</button>
      </center>
      </form>
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <center><h5 class="form-signin"><font color="black">Copyright </font> &copy; <a href="#" data-toggle="modal" data-target="#contact">Dana Pensiun</a></h5></center>
    </div> <!-- /container -->
    
    <!-- Modal Dialog Contact -->
<div class="modal fade" id="contact" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Contact Us</h4>
      </div>
      <div class="modal-body">
      Penggajian Karyawan adalah aplikasi penggajian karyawan 
      <table>
      <tr>
      <td>No Telepon</td> <td>:</td> <td></td>
      </tr>
      <br />
      <tr>
      <td>E-mail</td><td>:</td> <td><a href="mailto:dana.pensiun@gmail.com">dana.pensiun@gmail.com</a>|</td>
      </tr> 
      <br />
      <tr>
      </tr>
      <br />
      <tr>
      </tr>
       </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<!-- end dialog modal -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br /> 

      <!-- <div class="card-body">
        <form method="post" action="<?php echo base_url('index.php/auth/login') ?>">
          <div class="input-group form-group">
            <div class="input-group-prepend">
              <span class="input-group-text"><i class="fas fa-user"></i></span>
            </div>
            <input type="text" class="form-control" name="username" placeholder="username">
            
          </div>
          <div class="input-group form-group">
            <div class="input-group-prepend">
              <span class="input-group-text"><i class="fas fa-key"></i></span>
            </div>
            <input type="password" class="form-control" name="password" placeholder="password">
          </div>
          <div class="form-group">
            <input type="submit" value="Login" class="btn float-right login_btn">
          </div>
        </form>
      </div> -->
</div>
</body>
</html>