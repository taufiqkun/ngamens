<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('admin/head') ?>
<style>
.form-horizontal .control-label {
    text-align: left;
}
</style>

<body class="no-skin">
    <?php $this->load->view('admin/navbar') ?>

    <div class="main-container ace-save-state" id="main-container">
        <script type="text/javascript">
        try {
            ace.settings.loadState('main-container')
        } catch (e) {}
        </script>

        <?php $this->load->view('admin/sidebar') ?>

        <div class="main-content">
            <div class="main-content-inner">

                <div class="page-content">

                    <div class="page-header row">
                        <div class="col-sm-8">
                            <h1>
                                Data Ahli Waris
                            </h1>
                        </div>
                        <div class="col-sm-4">
                            <button class="btn btn-primary pull-right" data-toggle="modal" data-target="#tambah"
                                data-backdrop="static" data-keyboard="false"><i class="fa fa-plus"></i> Ahli Waris</button>
                        </div>
                    </div><!-- /.page-header -->



                    <div class="row">
                        <div class="col-xs-12">


                            <table id="datatable" class="table  table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Nama Pegawai</th>
                                        <th>Nama Ahli Waris</th>
                                        <th>Pekerjaan</th>
                                        <th>Status Nikah</th>
                                        <th>Hubungan</th>
                                        <th>#</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if($listAhliwaris!=""){
                		foreach ($listAhliwaris as $data) : 
            			?>

                                    <tr>
                                        <td></td>
                                        <td><?php echo $data->nama; ?></td>
                                        <td><?php echo $data->nama_ahliwaris; ?></td>
                                        <td><?php echo $data->pekerjaan; ?></td>
                                        <td><?php echo $data->status; ?></td>
                                        <td><?php echo $data->hubungan_keluarga; ?></td>

                                        <td>
                                            <div class="btn-group">
                                                <button class="btn btn-xs btn-info" title="edit"
                                                    id="<?php echo $data->id_waris; ?>"
                                                    onclick="getById(this.id)">
                                                    <i class="ace-icon fa fa-pencil bigger-120"></i>
                                                </button>&nbsp;

                                                <button class="btn btn-xs btn-danger title=" hapus"
                                                    id="<?php echo $data->id_waris; ?>" onclick="hapus(this.id)">
                                                    <i class="ace-icon fa fa-trash-o bigger-120"></i>
                                                </button>
                                            </div>


                                        </td>
                                    </tr>

                                    <?php
			                endforeach;
                        }
			            ?>
                                </tbody>
                            </table>





                            <!-- PAGE CONTENT ENDS -->
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.page-content -->
            </div>
        </div><!-- /.main-content -->


        <!-- Modal ADD AHLIWARIS-->
        <div id="tambah" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Tambah Ahli Waris</h4>
                    </div>
                    <form class="form-horizontal text-left" method="post" action="<?php echo base_url('ahliwaris/ahliwaris/add') ?>">
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Pilih Peserta :</label>
                            <div class="col-sm-8">
                                <select name="id_peserta" id="" require="" class="form-control" required="">
                                    <option value="" selected disabled>~Pilih Peserta~</option>
                                    <?php foreach ($listPeserta as $p) : ?>
                                        <option value="<?php echo $p->id_peserta; ?>"><?php echo $p->nama; ?></option>
                                    <?php endforeach;?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4 control-label">Nama Ahli Waris :</label>
                            <div class="col-sm-8">
                                <input class="form-control" placeholder="input nama..." name="namaAhliWaris" id="" 
                                    type="text" value="" required="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4 control-label">Nomor KTP/IDENTITAS :</label>
                            <div class="col-sm-8">
                                <input class="form-control" placeholder="No Identitas..." name="noIdentitasAhliWaris" id="" 
                                    type="text" value="" required="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4 control-label">Jenis Kelamin :</label>
                            <div class="col-sm-8">
                                <select name="gender" id="" require="" class="form-control" required="">
                                    <option value="" selected disabled>~Pilih Jenis Kelamin~</option>
                                    <option value="L">Laki Laki</option>
                                    <option value="P">Perempuan</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4 control-label">Status :</label>
                            <div class="col-sm-8">
                                <select name="statusAhliwaris" id="" require="" class="form-control" required="">
                                    <option value="" selected disabled>~Pilih Status Pernikahan~</option>
                                    <option value="1">Belum Menikah</option>
                                    <option value="2">Menikah</option>
                                    <option value="3">Janda/Duda</option>
                                    <option value="4">Cerai/Mati</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4 control-label">Pekerjaan :</label>
                            <div class="col-sm-8">
                                <input class="form-control" name="pekerjaanAhliWaris" id="" type="text" required >
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4 control-label">Pendidikan :</label>
                            <div class="col-sm-8">
                                <select name="pendidikanAhliwaris" id="" require="" class="form-control" required="">
                                    <option value="" selected disabled>~Pilih Pendidikan Terakhir~</option>
                                    <option value="TK">TK</option>
                                    <option value="SD">SD</option>
                                    <option value="SMP">SMP</option>
                                    <option value="SMA">SMA</option>
                                    <option value="D3">D3</option>
                                    <option value="S1">S1</option>
                                    <option value="S2">S2</option>
                                    <option value="S3">S3</option>
                                    <option value="no">Tidak Sekolah</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4 control-label">Tempat Lahir, Tgl :</label>
                            <div class="col-sm-8">
                                <div class="col-sm-6" style="margin-left: -12px">
                                    <input class="form-control" name="tempal_lahir" type="text" required >
                                </div> 

                                <div class="col-sm-6">                                   
                                    <input type="date" class="form-control" name="tglLahirAhliWaris" required="">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4 control-label">Hubungan Keluarga :</label>
                            <div class="col-sm-8">
                                <select name="hubunganAhliwaris" id="" require="" class="form-control" required="">
                                    <option value="" selected disabled>~Pilih Hubngan Keluarga~</option>
                                    <option value="OrangTua">Orang Tua</option>
                                    <option value="KakakKandung">Kakak Kandung</option>
                                    <option value="AdikKandung">Adik Kandung</option>
                                    <option value="Sepupu">Sepupu</option>
                                </select>
                            </div>
                        </div>

                    <div class="modal-footer">
                        <input type="submit" class="btn btn-primary pull-left" value="Simpan" >
                        <!-- <button type="submit" class="btn btn-primary pull-left" value="Simpan" id="simpanAhliWaris">Simpan</button> -->
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>

                </div>
                </form>


            </div>
        </div>
    </div>  
        <!-- END Modal ADD AHLIWARIS-->
        

        <!-- Modal Delete-->
        <div id="delete-modal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Konfirmasi Hapus</h4>
                    </div>
                    <form method="post"
                            action="<?php echo base_url('ahliwaris/ahliwaris/delete') ?>">
                    <div class="modal-body">
                        <h4 class="text-center"><i class="fa fa-warning red"></i> Yakin Hapus ?</h4>                           
                        <input type="hidden" name="id" id="id-hapus">  
                    </div>

                    <div class="modal-footer">
                        <input type="submit" class="btn btn-danger pull-left" value="Ya">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- End Hapus Edit -->

        <!-- Modal EDIT AHLIWARIS-->
        <div id="edit" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Edit Ahli Waris</h4>
                    </div>
                    <form class="form-horizontal text-left" method="post" action="<?php echo base_url('ahliwaris/ahliwaris/edit') ?>">
                    <div class="modal-body">                     

                        <input type="hidden" name="idAhliwaris" id="idAhliwaris">

                        <div class="form-group">
                            <label class="col-sm-4 control-label">Pilih Peserta :</label>
                            <div class="col-sm-8">
                                <select name="id_peserta" id="peserta" require="" class="form-control" required="">
                                    <?php foreach ($listPeserta as $p) : ?>
                                        <option value="<?php echo $p->id_peserta; ?>"><?php echo $p->nama; ?></option>
                                    <?php endforeach;?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4 control-label">Nama Ahli Waris :</label>
                            <div class="col-sm-8">
                                <input class="form-control" placeholder="input nama..." name="namaAhliWaris" id="namaAhliWaris" 
                                    type="text" value="" required="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4 control-label">Nomor KTP/IDENTITAS :</label>
                            <div class="col-sm-8">
                                <input class="form-control" placeholder="No Identitas..." name="noIdentitasAhliWaris" id="noIdentitasAhliWaris" 
                                    type="text" value="" required="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4 control-label">Jenis Kelamin :</label>
                            <div class="col-sm-8">
                                <select name="jkelAhliwaris" id="jkelAhliwaris" require="" class="form-control" required="">
                                    <option value="L">Laki Laki</option>
                                    <option value="P">Perempuan</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4 control-label">Status :</label>
                            <div class="col-sm-8">
                                <select name="statusAhliwaris" id="statusAhliwaris" require="" class="form-control" required="">
                                    <option value="1">Belum Menikah</option>
                                    <option value="2">Menikah</option>
                                    <option value="3">Janda/Duda</option>
                                    <option value="4">Cerai/Mati</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4 control-label">Pekerjaan :</label>
                            <div class="col-sm-8">
                                <input class="form-control" name="pekerjaanAhliWaris" id="pekerjaanAhliWaris" type="text" required >
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4 control-label">Pendidikan :</label>
                            <div class="col-sm-8">
                                <select name="pendidikanAhliwaris" id="pendidikanAhliwaris" require="" class="form-control" required="">
                                    <option value="TK">TK</option>
                                    <option value="SD">SD</option>
                                    <option value="SMP">SMP</option>
                                    <option value="SMA">SMA</option>
                                    <option value="D3">D3</option>
                                    <option value="S1">S1</option>
                                    <option value="S2">S2</option>
                                    <option value="S3">S3</option>
                                    <option value="no">Tidak Sekolah</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4 control-label">Tempat Lahir, Tgl :</label>
                            <div class="col-sm-8">
                                <div class="col-sm-4" style="margin-left: -12px">
                                    <input class="form-control" name="tempatLahirAhliWaris" id="tempatLahirAhliWaris" type="text" required >
                                </div>                                    
                                <div class="col-sm-6">                                   
                                    <input type="date" class="form-control"  name="tglLahirAhliWaris" id="tglLahirAhliWaris" required="">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4 control-label">Hubungan Keluarga :</label>
                            <div class="col-sm-8">
                                <select name="hubunganAhliwaris" id="hubunganAhliwaris" require="" class="form-control" required="">
                                    <option value="OrangTua">Orang Tua</option>
                                    <option value="KakakKandung">Kakak Kandung</option>
                                    <option value="AdikKandung">Adik Kandung</option>
                                    <option value="Sepupu">Sepupu</option>
                                </select>
                            </div>
                        </div>

                    <div class="modal-footer">
                        <input type="submit" class="btn btn-primary pull-left" value="Simpan" >
                        <!-- <button type="submit" class="btn btn-primary pull-left" value="Simpan" id="simpanAhliWaris">Simpan</button> -->
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>

                </div>
                </form>
            </div>
        </div>
    </div>
        <!-- END Modal ADD AHLIWARIS-->

        

<script type="text/javascript">
   function getById(id) {
    $.ajax({
        url: " <?php echo base_url('ahliwaris/ahliwaris/getById/') ?>"+ id,
        type: 'GET',
        

        success: function(result) {
            var res = $.parseJSON(result);            
            console.log(res)
            $('#idAhliwaris').val(res.id_waris)
            $('#id_peserta').val(res.id_peserta).change();  
            $('#namaAhliWaris').val(res.nama_ahliwaris)          
            $('#noIdentitasAhliWaris').val(res.noktp_ahliwaris)
            $('#jkelAhliwaris').val(res.jk_ahliwaris).change();
            $('#statusAhliwaris').val(res.status_nikah).change();
            $('#pekerjaanAhliWaris').val(res.pekerjaan)
            $('#pendidikanAhliwaris').val(res.pendidikan).change();
            $('#tempatLahirAhliWaris').val(res.kota_lahir_ahli_waris)
            $('#tglLahirAhliWaris').val(res.tanggal_lahir_ahli_waris)
            $('#hubunganAhliwaris').val(res.hubungan_keluarga).change();



            $('#edit').modal('show')
        },
        error: function() {
            alert('Error');
        }

    });
}

function hapus(id) {
    $('#id-hapus').val(id)
    $('#delete-modal').modal('show')
}






</script>
   <?php $this->load->view('admin/footer') ?>
</body>

</html>