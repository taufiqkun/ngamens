<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('admin/head') ?>
<style>
.form-horizontal .control-label {
    text-align: left;
}
</style>

<body class="no-skin">
    <?php $this->load->view('admin/navbar') ?>

    <div class="main-container ace-save-state" id="main-container">
        <script type="text/javascript">
        try {
            ace.settings.loadState('main-container')
        } catch (e) {}
        </script>

        <?php $this->load->view('admin/sidebar') ?>

        <div class="main-content">
            <div class="main-content-inner">

                <div class="page-content">

                    <div class="page-header row">
                        <div class="col-sm-8">
                            <h1>
                                Data Pensiun
                            </h1>
                        </div>

                        <div class="col-sm-4">
                            <button class="btn btn-primary pull-right" id="tambah" data-toggle="modal" data-target="#tambah_pensiun"
                                data-backdrop="static" data-keyboard="false">Tambah</button>
                        </div>
                    </div><!-- /.page-header -->



                    <div class="row">
                        <div class="col-xs-12">


                            <table id="datatable" class="table  table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Nama</th>
                                        <th>Tgl Pensiun</th>
                                        <th>Manfaat bln</th>
                                        <th>Pensiun bln</th>
                                        <th>Pensiun Total</th>
                                        <th>Keterangan</th>
                                        <th>#</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if($listPensiun!=""){
                		              foreach ($listPensiun as $data) : 
            			             ?>

                                    <tr>
                                        <td></td>
                                        <td><?php echo $data->nama; ?></td>
                                        <td><?php echo $data->tanggal_pensiun; ?></td>
                                        <td><?php echo $data->manfaat_bulanan; ?></td>
                                        <td><?php echo $data->pensiun_bulanan; ?></td>
                                        <td><?php echo $data->pensiun_total; ?></td>
                                        <td><?php echo $data->keterangan_manfaat; ?></td>
                                        <td>
                                            <div class="btn-group">
                                                <a href="<?php echo base_url("pensiun/pensiun/cetak/". $data->id_pensiun)?>" target="_blank" class="btn btn-xs btn-info" title="cetak PDF"
                                                    id="<?php echo $data->id_pensiun; ?>"
                                                    >
                                                    <i class="ace-icon fa fa-print bigger-120"></i>
                                                </a>&nbsp;

                                                <button class="btn btn-xs btn-danger title=" hapus"
                                                    id="<?php echo $data->id_pensiun; ?>" onclick="hapus(this.id)">
                                                    <i class="ace-icon fa fa-trash-o bigger-120"></i>
                                                </button>  
                                            </div>
                                        </td>
                                    </tr>

                                    <?php
			                endforeach;
                        }
			            ?>
                                </tbody>
                            </table>





                            <!-- PAGE CONTENT ENDS -->
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.page-content -->
            </div>
        </div><!-- /.main-content -->
       

        <!-- Modal -->
        <div id="tambah_pensiun" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Tambah Data Pensiun</h4>
                    </div>
                    <div class="modal-body">
                        <form class="form-horizontal text-left myform" method="post"
                            action="<?php echo base_url('pensiun/pensiun/add') ?>">
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Pilih Peserta :</label>
                                <div class="col-sm-8">
                                    <select class="form-control" name="id_peserta" required=""
                                        onchange="getPesertaById(this.value)">
                                        <option value="" selected disabled>Pilih Peserta...</option>
                                        <?php foreach ($listPeserta as $p) : ?>
                                        <option value="<?php echo $p->id_peserta; ?>"><?php echo $p->nama; ?></option>
                                        <?php endforeach;?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label">TMT masuk :</label>
                                <label class="col-sm-8 control-label" id="tmt_masuk"></label>
                                <div class="col-sm-8">
                                    <input name="tmt_masuk" id="tmt_masuk1" type="hidden">
                                </div>
                            </div>

                            <div class="form-group">
                                <?php  $date = date('Y-m-d');	?>
                                <label class="col-sm-4 control-label">Umur pada <?php echo $date; ?> :</label>
                                <label class="col-sm-8 control-label" id="usia"></label>
                                <div class="col-sm-8">
                                    <input name="usia" id="usia1" type="hidden">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label">Masa Kerja (th) :</label>
                                <label class="col-sm-8 control-label" id="masaKerja" name="masa"></label>
                                <div class="col-sm-8">
                                    <input name="masaKerja" id="masaKerja1" type="hidden">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label">Nilai Sekarang :</label>
                                <div class="col-sm-8">
                                    <input class="form-control" placeholder="input nilai Sekarang..." name="nilai" id="nilai"
                                        type="text" required="">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label">Faktor Sekaligus :</label>
                                <div class="col-sm-8">
                                    <input class="form-control" placeholder="input Faktor Sekaligus..."
                                        name="faktor_sekaligus" id="faktor" type="text" required="">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label">PhDP :</label>
                                <div class="col-sm-8">
                                    <input class="form-control" placeholder="input PhDP..." name="fhdp" id=""
                                        type="text" required="">
                                </div>
                            </div>  
                            <input type="hidden" name="status_nikah" id="status_nikah">
                            <input type="hidden" name="status_pegawai" id="status_pegawai">
                            <input type="hidden" name="tanggal_pensiun" id="tanggal_pensiun">
                    </div>
                    <div class="modal-footer">
                        <input type="submit" class="btn btn-primary pull-left" id="simpan" value="Simpan" >
                        
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>

                    
                </div>
                </form>
            </div>
        </div>
        <!-- END MODAL TAMBAH -->


        <!-- Modal Delete-->
        <div id="delete-modal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Konfirmasi Hapus</h4>
                    </div>
                    <div class="modal-body">
                        <form class="form-horizontal text-left" method="post"
                            action="<?php echo base_url('pensiun/pensiun/delete') ?>">
                            <p>Yakin Hapus ?</p>
                            <input type="hidden" name="id_pensiun" id="id-hapus">

                            <div class="modal-footer">
                                <input type="submit" class="btn btn-primary pull-left" value="Ya">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- End Modal Delete -->

        <script type="text/javascript">   

        function getPesertaById(id) {
            $.ajax({
                url: "<?php echo base_url('pensiun/pensiun/getById') ?>",
                type: 'POST',
                data: {
                    id: id
                },
                success: function(result) {
                    var res = $.parseJSON(result);
                    console.log(result)
                    $('#tmt_masuk').html(res.awal_tetap)
                    $('#usia').html(res.usia+" tahun")
                    $('#masaKerja').html(res.masaKerja+" tahun")
                    //masukkan hidden input
                    $('#tmt_masuk1').val(res.awal_tetap)
                    $('#usia1').val(res.usia)
                    $('#masaKerja1').val(res.masaKerja)
                    $('#status_nikah').val(res.status_nikah)
                    $('#tanggal_pensiun').val(res.tanggal_pensiun)
                    $('#status_pegawai').val(res.status_pegawai)

                    if( parseInt(res.masaKerja) < 0 ){
                        $('#simpan').attr('disabled', true);  
                    } else {
                        $('#simpan').attr('disabled', false);  
                    }
                },
                error: function() {
                    alert('Error');
                }

            });
        }

        function hapus(id) {
            $('#id-hapus').val(id)
            $('#delete-modal').modal('show')
        }

        function cetak(id) {
            $.ajax({
                url: "<?php echo base_url('pensiun/pensiun/cetak') ?>",
                type: 'POST',
                data: {
                    id: id
                },
                success: function(result) {
                    
                },
                error: function() {
                    alert('Error');
                }

            });
            // console.log(id);
        }

        $('#simpan').on('click', function(e) {
            if($('#nilai').val().includes(",") || $('#faktor').val().includes(",")){
                alert("format input tidak boleh menggunkaan tanda koma<','> ... ");           
                e.preventDefault();
            }
            
        })

        </script>

        <?php $this->load->view('admin/footer') ?>
</body>

</html>