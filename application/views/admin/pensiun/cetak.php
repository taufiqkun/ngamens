<html>
<!--[if !IE]> -->
<title><?php echo isset($title) ? $title : 'Ace Dashboard' ; ?></title>
<script src="<?php echo base_url('assets/template/back') ?>/js/jquery-2.1.4.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.5.3/jspdf.debug.js"></script>
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.0.272/jspdf.debug.js"></script>-->

<link href="https://fonts.googleapis.com/css?family=Open+Sans&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
<style type="text/css">
	body{
		font-family: 'Roboto', sans-serif;
		/*font-family: 'Open Sans', sans-serif;*/
	}
	h4 {
		margin-bottom: 0;
	}
</style>
<body id="content">
	
	<h3 style="text-align: center; margin-top: 40px">Perhitungan Pensiun</h3>
	<div style="margin: 40px">
		<table width="100%">
			<tr>
				<td width="200px"><h4>Nama Peserta</h4></td>
				<td><h4>: <?php echo $data['nama'] ?> </h4></td>
			</tr>
			<tr>
				<td width="200px"><h4>Nik</h4></td>
				<td><h4>: <?php echo $data['nik'] ?> </h4></td>
			</tr>
			<tr>
				<td width="200px"><h4>Status Pegawai</h4></td>
				<td><h4>: <?php echo $data['status_pegawai'] ?> </h4></td>
			</tr>
		 
		
		</table>
	</div>
	<hr>
	
	<div style="margin: 40px">
	<h4>Kalkulasi Besar Manfaat Pensiun :</h4>
	<table width="90%">
		<tr>
			<td width="40%">Tanggal lahir</td><td>:</td><td> <?php echo date('d F Y', strtotime($data['tanggal_lahir'])); ?></td><td width="30%"></td>
		</tr>
		<tr>
			<td>TMT Masuk IST AKPRIND</td><td>:</td><td><?php echo date('d F Y', strtotime($data['awal_masuk'])); ?> </td><td></td>
		</tr>
		<tr>
			<td>Usia</td><td>:</td><td><?php echo $data['usia'] ?> </td><td>tahun</td>
		</tr>
		<tr>
			<td>Masa Kerja (th)</td><td>:</td><td><?php echo $data['masaKerja'] ?> </td><td>tahun</td>
		</tr>
		<tr>
			<td>Nilai Sekarang</td><td>:</td><td><?php echo $data['nilai_sekarang'] ?>  </td>
		</tr>
		<tr>
			<td>Faktor Sekaligus</td><td>:</td><td><?php echo $data['faktor_sekaligus'] ?>  </td>
		</tr>
		<tr>
			<td>PHDP</td><td>:</td><td><b><?php echo $data['phdp'] ?> </b> </td>
		</tr>
	</table>

		<?php
		if ($data['keterangan']!="NORMAL") {
			?>
				<h4>Besar manfaat pensiun <?php echo $data['keterangan'] ?> (bulanan) = NS x (2% x MK x PhDP) </h4>
				<table width="90%">
					<tr>
						<td width="40%"><?php echo $data['nilai_sekarang'] ?> x (2% x <?php echo $data['masaKerja'] ?> x <?php echo $data['phdp'] ?>)</td><td><?php echo $data['besar_manfaat'] ?></td>
					</tr>
					<tr>
						<td>TMT PENSIUN</td><td><?php echo $data['timestamp'] ?></td>
					</tr>
				</table>

				<h4>Besar manfaat pensiun <?php echo $data['keterangan'] ?> (sekaligus) : <br>
					Faktor sekaligus x manfaat pensiun bulanan
				</h4>
			<?php
		}else{
			?>
				<h4>Besar manfaat pensiun <?php echo $data['keterangan'] ?> (bulanan) = NS x (2% x MK x PhDP) </h4>
				<table width="90%">
					<tr>
						<td width="40%"><?php echo $data['nilai_sekarang'] ?> x (2% x <?php echo $data['masaKerja'] ?> x <?php echo $data['phdp'] ?>)</td><td><?php echo $data['besar_manfaat'] ?></td>
					</tr>
					<tr>
						<td>TMT PENSIUN</td><td><?php echo $data['timestamp'] ?></td>
					</tr>
				</table>

				<h4>Besar manfaat pensiun <?php echo $data['keterangan'] ?> (sekaligus) : <br>
					Faktor sekaligus x manfaat pensiun bulanan
				</h4>
			<?php
		}
		?>

	<table width="90%">
		<tr>
			<td width="40%"><?php echo $data['faktor_sekaligus'] ?> x Rp <?php echo $data['besar_manfaat'] ?> </td><td>:</td><td><b><?php echo $data['faktor_sekaligus_bulanan'] ?></b></td><td width="30%"></td>
		</tr>
		<tr>
			<td>Jika 20% sebesar Rp </td><td>:</td><td><b><?php echo $data['faktor_sekaligus20'] ?></b></td><td><b><?php echo $data['persen20'] ?> </b> (pembulatan)</td>
		</tr>
		<tr>
			<td>80% dibayarkan bulanan Rp </td><td>:</td><td><b><?php echo $data['faktor_sekaligus80'] ?></b></td><td><b><?php echo $data['persen80'] ?> </b> (pembulatan)</td>
		</tr>

		<tr>
			<td>Pensiun Diterima Pertama Pada </td><td>:</td>
			<?php if ( $data['status_pegawai'] == 'Profesor'): ?>
        		<td colspan="2"><b>saat telah mencapai usia 70 tahun </b><td></td>
		    <?php elseif($data['status_pegawai'] == 'Dosen Tetap'): ?>
		        <td colspan="2"><b>saat telah mencapai usia 65 tahun </b><td></td>
	        <?php else: ?>
	        	<td colspan="2"><b>saat telah mencapai usia 56 tahun </b><td></td>
		    <?php endif; ?>
			
		</tr>
		<tr>			
			<td></td><td></td><td colspan="2"><b>pada <?php echo $data['tanggal_terima_pensiun'] ?></b></td>
		</tr>
		<!-- <tr>
			<td>Keterangan Pensiun </td><td>:</td><td colspan="2"><b><u><?php echo $data['keterangan'] ?></u></b></td>
		</tr> -->
	</table>

	<div style="float: right; margin-right: 40px; margin-top: 40px">
		<p>Yogyakarta, <?php echo $data['timestamp'] ?></p>
		<p>Direktur Administrasi dan Keuangan, </p>
		<br><br><br>
		<p><b>Catur Iswayudi, S.kom, S.E, M.Cs</b></p>
	</div>

<script type="text/javascript">
$(document).ready(function(){
    var doc =new jsPDF('p', 'pt', 'A4');
    //doc.addFont('ArialMS', 'Arial', 'normal');
	//doc.setFont('Arial');
    /*source = $('#content')[0];
    margins = {
            top: 80,
            bottom: 60,
            left: 40,
            width: 522
        };

    pdf.fromHTML(
            source, // HTML string or DOM elem ref.
            margins.left, // x coord
            margins.top, { // y coord
                'width': margins.width, // max width of content on PDF
                'elementHandlers': specialElementHandlers
            },

            function (dispose) {
                // dispose: object with X, Y of the last line add to the PDF 
                //          this allow the insertion of new lines after html
                pdf.save('Test.pdf');
            }, margins
        );*/
	doc.addHTML(document.body,function() {
	    doc.save('print.pdf');
	});
}) 
</script>
	
</body>
</html>

