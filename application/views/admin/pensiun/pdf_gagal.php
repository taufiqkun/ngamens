

<?php
    $pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
    $pdf->SetTitle('Data Peserta Pensiun');
    $pdf->SetHeaderMargin(30);
    $pdf->SetTopMargin(20);
    $pdf->setFooterMargin(20);
    $pdf->SetAutoPageBreak(true);
    $pdf->SetAuthor('Author');
    $pdf->SetDisplayMode('real', 'default');
    $pdf->AddPage();
    $i=0;



                                         
$html='<style type="text/css">
h4 {
    margin-bottom: 0;
}
</style>
<body >

<h3 style="text-align: center; margin-top: 40px">Perhitungan Pensiun</h3>
<h4 style="text-align: center;">Nama Peserta :'; echo $data['tanggal_lahir'];
$html.= '</h4>
    <div style="margin: 40px">
    <h4>Kalkulasi Besar Manfaat Pensiun :</h4>
    <table width="80%">
        <tr>
            <td>Tanggal lahir</td><td>:</td><td>'; echo $data['tanggal_lahir'] ;
            $html.='</td><td></td>
        </tr>
        <tr>
            <td>TMT Masuk IST AKPRIND</td><td>:</td><td>'; echo $data['awal_tetap'] ;
            $html.='</td><td></td>
        </tr>
        <tr>
            <td>Usia</td><td>:</td><td>'; echo $data['usia'] ;
            $html.=' </td><td>tahun</td>
        </tr>
        <tr>
            <td>Masa Kerja (th)</td><td>:</td><td>'; echo $data['masaKerja'] ;
            $html.='</td><td>tahun</td>
        </tr>
        <tr>
            <td>Nilai Sekarang</td><td>:</td><td>'; echo $data['nilai_sekarang']  ; 
            $html.='</td>
        </tr>
        <tr>
            <td>Faktor Sekaligus</td><td>:</td><td>'; echo $data['faktor_sekaligus'];
            $html.='</td>
        </tr>
        <tr>
            <td>PHDP</td><td>:</td><td><b>'; echo $data['phdp']; 
            $html.='</b> </td>
        </tr>
    </table>

    <h4>Besar manfaat pensiun Normal (bulanan) = 2% x MK x PhDP </h4>
    <table width="80%">
        <tr>
            <td>2% x '; echo $data['masaKerja']; $html.='x'; echo $data['phdp']; $html.='</td><td>'; echo $data['manfaat_bulanan'];
            $html.='</td>
        </tr>
        <tr>
            <td>TMT PENSIUN</td><td>'; echo $data['tanggal_pensiun'] ; $html.='</td>
        </tr>
    </table>

    <h4>Besar manfaat pensiun Normal (sekaligus) : <br>
        Faktor sekaligus x manfaat pensiun bulanan
    </h4>
    <table width="80%">
        <tr>
            <td>'; echo $data['faktor_sekaligus']; $html.= 'x Rp';  echo $data['manfaat_bulanan']; $html.='</td><td>:</td><td><b>'; echo $data['pensiun_bulanan']; $html.='</b></td><td></td>
        </tr>
        <tr>
            <td>Jika 20% sebesar Rp </td><td>:</td><td><b>'; echo $data['persen20'] ; $html.='</b></td><td><b>'; echo $data['persen20'] ; $html.='</b></td>
        </tr>
        <tr>
            <td>80% dibayarkan bulanan Rp </td><td>:</td><td><b>'; echo $data['persen80'] ; $html.='</b></td><td><b>'; echo $data['persen80']; $html.='</b></td>
        </tr>
        <tr>
            <td>Pensiun Diterima Pertama Pada </td><td>:</td><td><b>'; echo $data['tanggal_terima_pensiun'] ; $html.='</b></td><td></td>
        </tr>
    </table>

    <div style="float: right; margin-right: 100px">
        <p>Yogyakarta, </p>
        <p>Direktur Administrasi dan Keuangan, </p>
        <br><br><br>
        <p><b>Mas Ardy </b></p>
    </div>

    </body>';   

                
    $pdf->writeHTML($html, true, false, true, false, '');
    ob_end_clean();
    $pdf->Output('data_pensiun.pdf', 'I');
?>