<html>
<style type="text/css">
	h4 {
		margin-bottom: 0;
	}
</style>
<body>
      
<h4><?php echo $result['nama'] ?> </h4>

	

	<h3 style="text-align: center;">Perhitungan Pensiun</h3>
	<h4 style="text-align: center;">Nama Peserta : <?php echo $data['nama'] ?></h4>

	<div style="margin: 40px">
	<h4>Kalkulasi Besar Manfaat Pensiun :</h4>
	<table width="80%">
		<tr>
			<td>Tanggal lahir</td><td>:</td><td> <?php echo $data->tanggal_lahir ?></td><td></td>
		</tr>
		<tr>
			<td>TMT Masuk IST AKPRIND</td><td>:</td><td><?php echo $data->awal_tetap ?> </td><td></td>
		</tr>
		<tr>
			<td>Usia</td><td>:</td><td><?php echo $data->usia ?> </td><td>tahun</td>
		</tr>
		<tr>
			<td>Masa Kerja (th)</td><td>:</td><td><?php echo $data->masaKerja ?> </td><td>tahun</td>
		</tr>
		<tr>
			<td>Nilai Sekarang</td><td>:</td><td><?php echo $data->nilai_sekarang ?>  </td>
		</tr>
		<tr>
			<td>Faktor Sekaligus</td><td>:</td><td><?php echo $data->faktor_sekaligus ?>  </td>
		</tr>
		<tr>
			<td>PHDP</td><td>:</td><td><b><?php echo $data->phdp ?> </b> </td>
		</tr>
	</table>

	<h4>Besar manfaat pensiun Normal (bulanan) = 2% x MK x PhDP </h4>
	<table width="80%">
		<tr>
			<td>2% x <?php echo $data->masaKerja ?> x <?php echo $data->phdp ?></td><td><?php echo $data->manfaat_bulanan ?></td>
		</tr>
		<tr>
			<td>TMT PENSIUN</td><td><?php echo $data->tanggal_pensiun ?></td>
		</tr>
	</table>

	<h4>Besar manfaat pensiun Normal (sekaligus) : <br>
		Faktor sekaligus x manfaat pensiun bulanan
	</h4>
	<table width="80%">
		<tr>
			<td><?php echo $data->faktor_sekaligus ?> x Rp <?php echo $data->manfaat_bulanan ?> </td><td>:</td><td><b><?php echo $data->pensiun_bulanan ?></b></td><td></td>
		</tr>
		<tr>
			<td>Jika 20% sebesar Rp </td><td>:</td><td><b>1.2000</b></td><td><b>1000010</b></td>
		</tr>
		<tr>
			<td>80% dibayarkan bulanan Rp </td><td>:</td><td><b>1.2000</b></td><td><b>11111</b></td>
		</tr>
		<tr>
			<td>Pensiun Diterima Pertama Pada </td><td>:</td><td><b>1.2000</b></td><td></td>
		</tr>
	</table>

	<div style="float: right; margin-right: 100px">
		<p>Yogyakarta, </p>
		<p>Direktur Administrasi dan Keuangan, </p>
		<br><br><br>
		<p><b>Mas Ardy </b></p>
	</div>


	</div>

		
</body>
</html>

