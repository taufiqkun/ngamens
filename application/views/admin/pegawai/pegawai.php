<!DOCTYPE html>
<html lang="en">
	<?php $this->load->view('admin/head') ?>

	<body class="no-skin">
		<?php $this->load->view('admin/navbar') ?>

		<div class="main-container ace-save-state" id="main-container">
			<script type="text/javascript">
				try{ace.settings.loadState('main-container')}catch(e){}
			</script>

			<?php $this->load->view('admin/sidebar') ?>

			<div class="main-content">
				<div class="main-content-inner">				

					<div class="page-content">					

						<div class="page-header row">
							<div class="col-sm-8">
								<h1>Data Pegawai</h1>
							</div>							
						</div><!-- /.page-header -->

						<div class="row">
							<div class="col-xs-12">
								

                <table id="datatable" class="table  table-bordered table-hover">
					<thead>
						<tr>							
							<th>No.</th>
							<th>Nik</th>
							<th>Nama</th>
							<th>Tgl Lahir</th>
							<th>TMT</th>
							<th>Status</th>
							<th>Posisi</th>
							<th>Prodi</th>	
							<th>Unit Kerja</th>					
						</tr>
					</thead>
					<tbody>
						<?php
						if ($listPegawai!="") {
                		foreach ($listPegawai as $data) : //ngabsen data
            			?>

						<tr>
							<td></td>
							<td><?php echo $data->nik; ?></td>
							<td><?php echo $data->nama; ?></td>
							<td><?php echo $data->tanggal_lahir; ?></td>
							<td><?php echo $data->awal_tetap; ?></td>
							<td>
								<?php if($data->status_nikah == 1 ): ?>
									Belum Menikah
								<?php elseif ($data->status_nikah == 2): ?>
									Menikah
								<?php elseif ($data->status_nikah == 2): ?>
									Janda/Duda
								<?php else: ?>
									-
								<?php endif; ?>

							</td>
							<td><?php echo $data->status_pegawai; ?></td>
							<td><?php echo $data->prodi; ?></td>
							<td><?php echo $data->unit_kerja; ?></td>						

							<!--<td>
								<div class="hidden-sm hidden-xs btn-group">
									<button class="btn btn-xs btn-success">
										<i class="ace-icon fa fa-plus bigger-120"></i>
									</button>

									<button class="btn btn-xs btn-info">
										<i class="ace-icon fa fa-pencil bigger-120"></i>
									</button>

									<button class="btn btn-xs btn-danger">
										<i class="ace-icon fa fa-trash-o bigger-120"></i>
									</button>									
								</div>

								
							</td>-->
						</tr>

						<?php
			                endforeach;
			               }
			            ?>
					</tbody>
				</table>


								<!-- PAGE CONTENT ENDS -->
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->

			<?php $this->load->view('admin/footer') ?>
	</body>
</html>


