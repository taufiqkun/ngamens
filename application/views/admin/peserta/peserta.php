<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('admin/head') ?>
<style>
.form-horizontal .control-label {
    text-align: left;
}
</style>

<body class="no-skin">
    <?php $this->load->view('admin/navbar') ?>

    <div class="main-container ace-save-state" id="main-container">
        <script type="text/javascript">
        try {
            ace.settings.loadState('main-container')
        } catch (e) {}
        </script>

        <?php $this->load->view('admin/sidebar') ?>

        <div class="main-content">
            <div class="main-content-inner">

                <div class="page-content">

                    <div class="page-header row">
                        <div class="col-sm-8">
                            <h1>
                                Data Peserta
                            </h1>
                        </div>

                        <div class="col-sm-4">
                            <button class="btn btn-primary pull-right" data-toggle="modal" data-target="#myModal"
                                data-backdrop="static" data-keyboard="false">Tambah Peserta</button>
                        </div>
                    </div><!-- /.page-header -->



                    <div class="row">
                        <div class="col-xs-12">


                            <table id="datatable" class="table  table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>No Peserta</th>
                                        <th>Pegawai</th>
                                        <th>tgl Pensiun</th>
                                        <th>tgl Terima Pensiun</th>
                                        <th>Status</th>
                                        <th>#</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if($listPeserta!=""){
                		foreach ($listPeserta as $data) : 
                            $stt='';
                            foreach ($id_peserta_from_ah as $id) {
                                
                                if ($data->id_peserta==$id->id_peserta) {
                                    $stt='disabled';
                                }
                            }
                            
            			?>


                                    <tr>
                                        <td></td>
                                        <td><?php echo $data->no_peserta; ?></td>
                                        <td><?php echo $data->nama; ?></td>
                                        <td><?php echo $data->tanggal_pensiun; ?></td>
                                        <td><?php echo $data->tanggal_terima_pensiun; ?></td>
                                        <td><?php echo $data->status_peserta; ?></td>

                                        <td>
                                            <div class="btn-group">
                                                <button class="btn btn-xs btn-info" title="edit"
                                                    id="<?php echo $data->id_peserta; ?>"
                                                    onclick="getPesertaById(this.id)">
                                                    <i class="ace-icon fa fa-pencil bigger-120"></i>
                                                </button>&nbsp;

                                                <button class="btn btn-xs btn-danger title=" hapus"
                                                    id="<?php echo $data->id_peserta; ?>" <?php echo $stt; ?> onclick="hapus(this.id)">
                                                    <i class="ace-icon fa fa-trash-o bigger-120"></i>
                                                </button>

                                                <button class="btn btn-xs btn-success title=" AhliWaris"
                                                    id="<?php echo $data->id_peserta; ?>" <?php echo $stt; ?> onclick="addahliwaris(this.id)">
                                                    <i class="ace-icon fa fa-plus bigger-120" >Ahli Waris</i>
                                                </button>
                                            </div>


                                        </td>
                                    </tr>

                                    <?php
			                endforeach;
                        }
			            ?>
                                </tbody>
                            </table>





                            <!-- PAGE CONTENT ENDS -->
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.page-content -->
            </div>
        </div><!-- /.main-content -->



        <!-- Modal ADD PERSERTA -->
        <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Tambah Pegawai</h4>
                    </div>

                    <form class="form-horizontal text-left" method="post"
                            action="<?php echo base_url('peserta/peserta/add') ?>">

                        <div class="modal-body">
                            <div class="form-group">
                                <label class="col-sm-4 control-label">No Peserta :</label>
                                <div class="col-sm-8">
                                    <input class="form-control" placeholder="input no peserta..." name="no_peserta"
                                        type="text" value="" required="">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label">Pilih Pegawai :</label>
                                <div class="col-sm-8">
                                    <select class="form-control chosen-select" name="id_pegawai" onchange="getumur(this.value)">
                                        <option value="" selected disabled>Pilih Pegawai...</option>
                                        <?php foreach ($listPegawai as $p) : ?>
                                        <option value="<?php echo $p->id_pegawai; ?>"><?php echo $p->nama; ?></option>
                                        <?php endforeach;?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label">Status Peserta :</label>
                                <div class="col-sm-8">
									<select name="status_peserta" require="" class="form-control" required="">
										<option value="" selected disabled>~Pilih Status~</option>
										<option value="Aktif">Aktif</option>
										<option value="Tidak Aktif">Tidak Aktif</option>
									</select>
                                    <!-- <input class="form-control" placeholder="input status peserta..."
                                        name="status_peserta" type="text" value="" required=""> -->
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label">Tgl Pensiun :</label>
                                <div class="col-sm-8">
                                    <!--<label class="col-sm-4 control-label" id="tanggal_pensiun"></label>-->
                                    <input class="form-control" name="tanggal_pensiun" type="text" id="tanggal_pensiun" readonly >
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label">Tgl Terima Pensiun :</label>
                                <div class="col-sm-8">
                                    <input class="form-control" name="tanggal_terima_pensiun" id="tanggal_terima_pensiun" type="text" readonly >
                                </div>
                            </div>
                        </div>

                        <div class="modal-footer">
                            <input type="submit" class="btn btn-primary pull-left" value="Simpan" >
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </form>    
                </div>
                
            </div>
        </div>           

        <!-- Modal Edit-->
        <div id="edit-modal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
               
                <div class="modal-content">
                    <form class="form-horizontal text-left" method="post"
                            action="<?php echo base_url('peserta/peserta/update') ?>">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Edit Pegawai</h4>
                    </div>

                    <div class="modal-body">
                        
                            <input type="hidden" name="id_peserta" id="id_peserta">
                            <div class="form-group">
                                <label class="col-sm-4 control-label">No Peserta :</label>
                                <div class="col-sm-8">
                                    <input class="form-control" placeholder="input no peserta..." name="no_peserta"
                                        id="no" type="text" value="" required="">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label">Pilih Pegawai :</label>
                                <div class="col-sm-8">
                                    <select class="form-control" name="id_pegawai" id="id_pegawai" required="">                                  
                                    </select>                                    
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label">Status Peserta :</label>
                                <div class="col-sm-8">
                                    <select name="status_peserta" require="" class="form-control" id="status" required="">
                                        <option value="" selected>~Pilih Status~</option>
                                        <option value="Aktif">Aktif</option>
                                        <option value="Tidak Aktif">Tidak Aktif</option>
                                    </select>                                    
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label">Tgl Pensiun :</label>
                                <div class="col-sm-8">
                                    <input class="form-control" id="tglPensi"
                                        name="tanggal_pensiun" type="text" value="" readonly>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label">Tgl Terima Pensiun :</label>
                                <div class="col-sm-8">
                                    <input class="form-control" id="tgl-trm-pensi"
                                        name="tanggal_terima_pensiun" type="text" value="" readonly>
                                </div>
                            </div>
                    </div>

                    <div class="modal-footer">
                        <input type="submit" class="btn btn-primary pull-left" value="Simpan">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </form>
                </div>
                
            </div>
        </div>
        <!-- End Modal Edit -->

        <!-- Modal Delete-->
        <div id="delete-modal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Konfirmasi Hapus</h4>
                    </div>
                    <div class="modal-body">
                        <form class="form-horizontal text-left" method="post"
                            action="<?php echo base_url('peserta/peserta/delete') ?>">
                            <p>Yakin Hapus ?</p>
                            <input type="hidden" name="id_peserta" id="id-hapus">

                            <div class="modal-footer">
                                <input type="submit" class="btn btn-primary pull-left" value="Ya">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </form>
                    </div>
                    
                </div>
            </div>
        </div>
        <!-- End Hapus Edit -->

        <!-- Modal ADD AHLIWARIS-->
        <div id="addahliwaris-modal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Tambah Ahli Waris</h4>
                    </div>
                    <form class="form-horizontal text-left" method="post" action="<?php echo base_url('peserta/peserta/addahliwaris') ?>">
                    <div class="modal-body">                        

                        <input type="hidden" name="idPeserta" id="idPeserta">

                        <div class="form-group">
                            <label class="col-sm-4 control-label">Nama Ahli Waris :</label>
                            <div class="col-sm-8">
                                <input class="form-control" placeholder="input nama..." name="namaAhliWaris" id="namaAhliWaris" 
                                    type="text" value="" required="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4 control-label">Nomor KTP/IDENTITAS :</label>
                            <div class="col-sm-8">
                                <input class="form-control" placeholder="No Identitas..." name="noIdentitasAhliWaris" id="noIdentitasAhliWaris" 
                                    type="text" value="" required="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4 control-label">Jenis Kelamin :</label>
                            <div class="col-sm-8">
                                <select name="jkelAhliwaris" id="jkelAhliwaris" require="" class="form-control" required="">
                                    <option value="" selected disabled>~Pilih Jenis Kelamin~</option>
                                    <option value="L">Laki Laki</option>
                                    <option value="P">Perempuan</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4 control-label">Status :</label>
                            <div class="col-sm-8">
                                <select name="statusAhliwaris" id="statusAhliwaris" require="" class="form-control" required="">
                                    <option value="" selected disabled>~Pilih Status Pernikahan~</option>
                                    <option value="1">Belum Menikah</option>
                                    <option value="2">Menikah</option>
                                    <option value="3">Janda/Duda</option>
                                    <option value="4">Cerai/Mati</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4 control-label">Pekerjaan :</label>
                            <div class="col-sm-8">
                                <input class="form-control" name="pekerjaanAhliWaris" id="pekerjaanAhliWaris" type="text" required >
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4 control-label">Pendidikan :</label>
                            <div class="col-sm-8">
                                <select name="pendidikanAhliwaris" id="pendidikanAhliwaris" require="" class="form-control" required="">
                                    <option value="" selected disabled>~Pilih Pendidikan Terakhir~</option>
                                    <option value="TK">TK</option>
                                    <option value="SD">SD</option>
                                    <option value="SMP">SMP</option>
                                    <option value="SMA">SMA</option>
                                    <option value="D3">D3</option>
                                    <option value="S1">S1</option>
                                    <option value="S2">S2</option>
                                    <option value="S3">S3</option>
                                    <option value="no">Tidak Sekolah</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4 control-label">Tempat Lahir, Tgl :</label>
                            <div class="col-sm-8">
                                <div class="col-sm-4" style="margin-left: -12px">
                                    <input class="form-control" name="tempatLahirAhliWaris" id="tempatLahirAhliWaris" type="text" required >
                                </div>                                    
                                <input type="date" name="tglLahirAhliWaris" required="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4 control-label">Hubungan Keluarga :</label>
                            <div class="col-sm-8">
                                <select name="hubunganAhliwaris" id="hubunganAhliwaris" require="" class="form-control" required="">
                                    <option value="" selected disabled>~Pilih Hubngan Keluarga~</option>
                                    <option value="OrangTua">Orang Tua</option>
                                    <option value="KakakKandung">Kakak Kandung</option>
                                    <option value="AdikKandung">Adik Kandung</option>
                                    <option value="Sepupu">Sepupu</option>
                                </select>
                            </div>
                        </div>

                    <div class="modal-footer">
                        <input type="submit" class="btn btn-primary pull-left" value="Simpan" >
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>

                </div>
                </form>
            </div>
        </div>
        <!-- END Modal ADD AHLIWARIS-->

<script type="text/javascript">
   function getPesertaById(id) {
    $.ajax({
        url: "<?php echo base_url('peserta/peserta/getById') ?>",
        type: 'POST',
        data: {
            id: id
        },
        success: function(result) {
            var res = $.parseJSON(result);            
            $('#id_peserta').val(res.id_peserta)
            $('#no').val(res.no_peserta)
            $('#id_pegawai').html('<option value="'+res.id_pegawai+'">'+res.nama+'</option>');
            $('#status').val(res.status_peserta).change();
            $('#tglPensi').val(res.tanggal_pensiun)
            $('#tgl-trm-pensi').val(res.tanggal_terima_pensiun)

            $('#edit-modal').modal('show')
        },
        error: function() {
            alert('Error');
        }

    });
}

function hapus(id) {
    $('#id-hapus').val(id)
    console.log(id)

    /*cek wes eneng neng pensiun rung*/
    $.ajax({
        url: "<?php echo base_url('peserta/peserta/cekPensiun') ?>",
        type: 'POST',
        data: {
            id: id
        },
        success: function(result) {
            var res = $.parseJSON(result);
            console.log(res)
            if (res.onora=="ok") {
                alert('Data Tidak Dapat Dihapus, karena sudah terdaftar sebagai pensiunan')
                // $('#notif').modal('show')
            } else {
                $('#delete-modal').modal('show')
            }
        },
        error: function() {
            alert('Error');
        }
    });
   
}

function getumur(id){
    $.ajax({
        url: "<?php echo base_url('peserta/peserta/getTglPensiun') ?>",
        type: 'POST',
        data: {
            id: id
        },
        success: function(result) {
            var res = $.parseJSON(result);
            console.log(res)
            $('#tanggal_pensiun').val(res.tgl_pensiun)
            $('#tanggal_terima_pensiun').val(formatDate(res.tgl_pensiun));
        },
        error: function() {
            alert('Error');
        }
    });
}

function formatDate(date) {
    var d = new Date(date);
    d.setMonth(d.getMonth()+1)
    month = (d.getMonth() + 1)
    day = d.getDate()
    year = d.getFullYear()    
    if (parseInt(month) < 10) month = '0' + month;
    if (parseInt(day) < 10) day = '0' + day;

    return [year, month, day].join('-');
}

var id_peserta;
function addahliwaris(id){
    this.id_peserta=id;
    console.log(id)
    $('#idPeserta').val(id)    
    $('#addahliwaris-modal').modal('show')
    console.log(id_peserta);
}

$('#simpanAhliWaris').click(function(){
    var data ={
        'id_peserta' : document.getElementById("idPeserta").value,
        'nama_ahliwaris' : document.getElementById("namaAhliWaris").value,
        'noktp_ahliwaris' : document.getElementById("noIdentitasAhliWaris").value,
        'jk_ahliwaris' : document.getElementById("jkelAhliwaris").value,
        'status_nikah' : document.getElementById("statusAhliwaris").value,
        'pekerjaan' : document.getElementById("pekerjaanAhliWaris").value,
        'pendidikan' : document.getElementById("pendidikanAhliwaris").value,
        'kota_lahir_ahli_waris' : document.getElementById("tempatLahirAhliWaris").value,
        'tanggal_lahir_ahli_waris' : document.getElementById("tglLahirAhliWaris").value,
        'hubungan_keluarga' : document.getElementById("hubunganAhliwaris").value
    };
    console.log(data);
    // $.ajax({
    //     url: "<?php //echo base_url('peserta/peserta/addahliwaris') ?>",
    //     type: "POST",
    //     data : data,
    //       success:function(data){
    //         console.log(data);
    //         var res = $.parseJSON(data);
    //           if(res.status == true){               
    //             alert('Data Paket Berhasil Dihapus');
    //             // window.location.href = 'paket.php';
    //           }else{
    //             alert('Gagal Hapus Paket');
    //             // window.location.href = 'paket.php';
    //           } 
    //         }
    // });
});

</script>
   <?php $this->load->view('admin/footer') ?>
</body>

</html>