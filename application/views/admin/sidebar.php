<div id="sidebar" class="sidebar                  responsive                    ace-save-state">
  <script type="text/javascript">
    try{ace.settings.loadState('sidebar')}catch(e){}
  </script>

  <ul class="nav nav-list">
    <li>
      <a href="<?php echo base_url('home/index') ?>">
        <i class="menu-icon fa fa-tachometer"></i>
        <span class="menu-text"> Dashboard </span>
      </a>

      <b class="arrow"></b>
    </li>
   

    <li class="<?php if ($title=='Pegawai'){echo 'active';} ?>">
      <a href="<?php echo base_url('/pegawai/pegawai') ?>">
        <i class="menu-icon fa fa-list"></i>
        <span class="menu-text"> Data Pegawai </span>        
      </a>     
    </li>

    <li class="<?php if ($title=='Peserta'){echo 'active';} ?>">
      <a href="<?php echo base_url('/peserta/peserta') ?>">
        <i class="menu-icon fa fa-list"></i>
        <span class="menu-text"> Data Peserta </span>        
      </a>     
    </li>

    <li class="<?php if ($title=='Pensiun'){echo 'active';} ?>">
      <a href="<?php echo base_url('/pensiun/pensiun') ?>">
        <i class="menu-icon fa fa-list"></i>
        <span class="menu-text"> Mgt Pensiun </span>        
      </a>     
    </li>

    <li class="<?php if ($title=='ahliwaris'){echo 'active';} ?>">
      <a href="<?php echo base_url('/ahliwaris/ahliwaris') ?>">
        <i class="menu-icon fa fa-list"></i>
        <span class="menu-text"> Data Ahliwaris </span>        
      </a>     
    </li>
    
    
  </ul><!-- /.nav-list -->

  <div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
    <i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
  </div>
</div>
